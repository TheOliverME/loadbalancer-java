package LoadBalancer.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import LoadBalancer.Main.Logger;
import LoadBalancer.Main.Variables;

public class ServerClient implements Runnable {

	private Socket s;

	public ServerClient(Socket s){
		
		this.s = s;
		
	}

	@Override
	public void run() {

		Logger.log("New client connection! IP: " + s.getInetAddress().getHostName());
		
		try {
			
			InputStream in = s.getInputStream();
			OutputStream out = s.getOutputStream();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			PrintWriter writer = new PrintWriter(out);
			
			if(!reader.readLine().equals(Variables.PASSWORD)){
				s.close();
				Logger.warn("Client wrong password! IP: " + s.getInetAddress().getHostName());
			}
			
			while(!s.isClosed()){
				
				Logger.log(reader.readLine());
				
			}
			
		} catch (IOException e) {
			Logger.err("Client error!");
		}
	
	}
	
}
