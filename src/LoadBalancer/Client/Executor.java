package LoadBalancer.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import LoadBalancer.Main.Logger;

public class Executor extends Logger {

	public static ArrayList<String> execCmd(String cmd) throws IOException, InterruptedException {
        
		Runtime rt = Runtime.getRuntime();
		Process proc = rt.exec(cmd);

		proc.waitFor();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

		ArrayList<String> returns = new ArrayList<String>();
		String cache;
		while ((cache = reader.readLine()) != null) {
		    returns.add(cache);
		}
        
		return returns;
		
    }
	
}
