package LoadBalancer.Main;

import LoadBalancer.Server.APIServerHandler;

public class Main extends Logger {

	static APIServerHandler server = new APIServerHandler();
	
	public static void main(String[] args) {
		
		Thread d = new Thread(new MainThread());
		d.start();

	}

	static class MainThread implements Runnable{
	
		@Override
		public void run() {

			log("Starting Loadbalancer!");
			if(!UseCheck.isIptables()){ err("Iptables not found!"); return; }
			log("Starting socket server! port: 2311");
			server.start();
			
		}
	
	}
}
