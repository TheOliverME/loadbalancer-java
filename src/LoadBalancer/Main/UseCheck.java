package LoadBalancer.Main;

import java.io.IOException;

import LoadBalancer.Client.Executor;

public class UseCheck {

	public static boolean isIptables(){
		
		try {
			for(String r : Executor.execCmd("iptables --version")){
				
				if(r.startsWith("iptables v")){
					return true;
				}
				
			}
		} catch (IOException | InterruptedException e) {
			return false;
		}
		
		return false;
		
	}
	
}
