package LoadBalancer.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import LoadBalancer.Client.ServerClient;
import LoadBalancer.Main.Logger;
import LoadBalancer.Main.Variables;

public class APIServerHandler {

	Thread d;
	
	public void start(){
		
		if(d != null){ Logger.warn("Thread already running!"); return; }
		
		d = new Thread(new ServerThread(Variables.PORT));
		d.start();
		
		
	}
	
	public void stop(){
		
		d.interrupt();
		d = null;
		
	}
	
	class ServerThread implements Runnable {

		private int PORT;

		public ServerThread(int port){
			this.PORT = port;
		}
		
		@Override
		public void run() {
			
			Logger.log("Starting Server Socket!");
			try {
				
				ServerSocket srv = new ServerSocket(PORT);
				
				while(true){
				
				Socket client = srv.accept();
				
				Thread d = new Thread(new ServerClient(client));
				d.start();
				
				}
				
			} catch (IOException e) {
				Logger.err("An error occured");
				e.printStackTrace();
			}
			
		}
		
	}
	
}
