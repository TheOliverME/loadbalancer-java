# LoadBalancing making easy with Java! #

This loadbalancer is working with iptables. You can connet via a socket connection to the daemon and create new projects and managing all balancing methods!

### How To Install ###

First thing you need is a server, which has a good internet connection worldwide! I prefer DigitalOcean server in Frankfurt. Then you install Java, tmux and iptables. 


```
#!bash 

apt-get install iptables openjdk-headless-7 tmux
```


After this you can clone this git repo or download this via your browser and upload it on your server. Now you create a new session with tmux.


```
#!bash

tmux new -s LoadBalancer-Java
```

Then you navigate to the file and start it!


```
#!bash

java -jar -Xms2G -Xmx2G LoadBalancer.jar
```

After this you can detach the virtual console with CRTL + B and D. Now access the loadbalancing server via telnet or other programms.


### How To Control The Daemon ###
Currently there is no software to control the daemon visually. But an approved easy way to connect to the daemon is telnet. You can access the daemon worldwide! Let's try it!


```
#!bash

telnet 127.0.0.1 2311
```

After this you have to send the password for authentification. Now you can send commands to the daemon and controll it. Read more in the commandlist section.